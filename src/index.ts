import AWS from "aws-sdk";
import express from "express";
import Fuse from "fuse.js";
import mongoose from "mongoose";
import TelegramBot from "node-telegram-bot-api";
import { log } from "util";
import * as handlers from "./handlers/soundManagingHandler";
import { Sound } from "./models/Sound";
import { States, User } from "./models/User";
import logger from "./utils/logger";
import { initEnv } from "./utils/secrets";
import { handleMessage } from "./utils/tgPromiseHandler";

initEnv();

export const app = express();
const port: number = parseInt(process.env.PORT as string, 10);
export const bot = new TelegramBot(process.env.BOTTOKEN as string, {
  polling: true
});
export const s3 = new AWS.S3({
  accessKeyId: process.env.CLOUDCUBE_ACCESS_KEY_ID,
  secretAccessKey: process.env.CLOUDCUBE_SECRET_ACCESS_KEY
});

mongoose
  .connect(process.env.MONGODB_URI as string)
  .then(() => logger.debug("Mongoose connected successfully"))
  .catch(err => logger.emerg("Mongoose connection error", err));

bot.on("polling_error", err => logger.error(err));

bot.on("message", (msg: TelegramBot.Message) => {
  User.findOne({
    chatId: msg.chat.id
  })
    .populate({
      path: "sounds"
    })
    .then(user => {
      if (user !== null) {
        bot.sendMessage(
          user.chatId,
          `Hey, I know you ! You are ${user.username}`
        );
        switch (user.state) {
          case States.NONE:
            handleMessage(handlers.HandleStateNone(user, msg), msg.chat.id);
            break;
          case States.WAITING_FOR_ORIGIN:
            handleMessage(handlers.HandleStateOrigin(user, msg), msg.chat.id);
            break;
          case States.WAITING_FOR_NAME:
            handleMessage(handlers.HandleStateName(user, msg), msg.chat.id);
            break;
          case States.WAITING_FOR_AUDIO:
            handleMessage(handlers.HandleStateAudio(user, msg), msg.chat.id);
            break;
          case States.WAITING_FOR_TRANSCRIPTION:
            handleMessage(
              handlers.HandleStateTranscript(user, msg),
              msg.chat.id
            );
            break;
        }
      } else {
        bot.sendMessage(
          msg.chat.id,
          "Hey, I don't know you yet! Don't worry. I PROMISE i will remember you !"
        );
        const newUser = new User({
          username: msg.chat.username,
          chatId: msg.chat.id
        });
        newUser
          .save()
          .then(() => {
            bot.sendMessage(
              msg.chat.id,
              "Promise RESOLVED. Yay ! Let's have fun !"
            );
          })
          .catch(err => {
            bot.sendMessage(
              msg.chat.id,
              "Sometimes I just must REJECT my promises..."
            );
            logger.error(err);
          });
      }
    })
    .catch(err => {
      bot.sendMessage(
        msg.chat.id,
        "OOPS ! Something went wrong when I tried to get familiar with you !"
      );
      logger.error(err);
    });
  logger.info("got message");
});

bot.on("inline_query", (query: TelegramBot.InlineQuery) => {
  logger.info("Got inline query");
  Sound.find().then(sounds => {
    const fuse = new Fuse(sounds, {
      shouldSort: true,
      tokenize: true,
      threshold: 0.4,
      location: 0,
      distance: 100,
      maxPatternLength: 256,
      minMatchCharLength: 1,
      keys: ["transcription", "origin", "name"]
    });
    const results = fuse.search(query.query);

    const answerResults = [] as TelegramBot.InlineQueryResultAudio[];

    for (const fuzzResult of results) {
      answerResults.push({
        type: "audio",
        id: fuzzResult._id,
        // eslint-disable-next-line @typescript-eslint/camelcase
        audio_url: fuzzResult.location,
        title: fuzzResult.name,
        performer: fuzzResult.origin
      });
    }

    bot.answerInlineQuery(query.id, answerResults);
  });
});

app.listen(port, () => {
  log("gotcha");
});
