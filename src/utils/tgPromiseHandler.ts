import { bot } from "../index";
import logger from "./logger";

export function handleMessage(promise: Promise<string>, chatId: number): void {
  promise
    .then(message => bot.sendMessage(chatId, message))
    .catch(message => {
      bot.sendMessage(chatId, message);
      logger.error(message, chatId);
    });
}
