import dotenv, { DotenvConfigOutput } from "dotenv";
import fs from "fs";
import logger from "./logger";

export function initEnv(): void {
  let output: DotenvConfigOutput;

  if (fs.existsSync(".env")) {
    logger.debug("Using .env file to supply config environment variables");
    output = dotenv.config({ path: ".env" });
  } else {
    logger.debug(
      "Using .env.example file to supply config environment variables"
    );
    output = dotenv.config({ path: ".env.example" });
  }

  if (output.error) {
    logger.error(output.error.message);
    process.exit(1);
  } else {
    logger.info(".env OK");
  }
}
