import * as S3 from "aws-sdk/clients/s3";
import TelegramBot from "node-telegram-bot-api";
import { bot, s3 } from "../index";
import { Sound, STATUS_CREATED, STATUS_CREATING } from "../models/Sound";
import { States, UserDocument } from "../models/User";
import logger from "../utils/logger";

export function HandleStateNone(
  user: UserDocument,
  msg: TelegramBot.Message
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    if (msg.text === "/add") {
      if (user.isManager) {
        const creatingSound = new Sound({
          manager: user._id
        });
        creatingSound.save().catch(err => logger.error(err));

        user.state = States.WAITING_FOR_ORIGIN;
        user.sounds.push(creatingSound._id);
        user.save().catch(err => logger.error(err));

        resolve("Great, let's do this ! Give me an origin of this sound");
      } else {
        reject("Sorry, can't let you do that.");
      }
    } else {
      reject("I don't understand you !");
    }
  });
}

export function HandleStateOrigin(
  user: UserDocument,
  msg: TelegramBot.Message
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    if (msg.text) {
      if (user.isManager) {
        const creatingSound = user.sounds.find(
          sound => sound.status === STATUS_CREATING
        );

        if (creatingSound) {
          creatingSound.origin = msg.text;
          creatingSound.save();
          user.state = States.WAITING_FOR_NAME;
          user.save();
        } else {
          user.state = States.NONE;
          user.save();
          reject("No sound is currently creating");
        }

        resolve("Cool ! Now give me a name of this sound");
      } else {
        user.state = States.NONE;
        user.save();
        reject("Sorry, can't let you do that.");
      }
    } else {
      reject("It is not a text at all.");
    }
  });
}

export function HandleStateName(
  user: UserDocument,
  msg: TelegramBot.Message
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    if (msg.text) {
      if (user.isManager) {
        const creatingSound = user.sounds.find(
          sound => sound.status === STATUS_CREATING
        );

        if (creatingSound) {
          creatingSound.name = msg.text;
          creatingSound.save();
          user.state = States.WAITING_FOR_AUDIO;
          user.save();
        } else {
          user.state = States.NONE;
          user.save();
          reject("No sound is currently creating");
        }

        resolve("Nice. Now gimme that sweet-sweet sound of yours!");
      } else {
        user.state = States.NONE;
        user.save();
        reject("Sorry, can't let you do that.");
      }
    } else {
      reject("It is not a text at all.");
    }
  });
}

export function HandleStateAudio(
  user: UserDocument,
  msg: TelegramBot.Message
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    if (msg.audio) {
      if (user.isManager) {
        const creatingSound = user.sounds.find(
          sound => sound.status === STATUS_CREATING
        );

        if (creatingSound) {
          const stream = bot.getFileStream(msg.audio.file_id);

          s3.upload(
            {
              Bucket: process.env.CLOUDCUBE_BUCKET as string,
              Key: `${process.env.CLOUDCUBE_CUBE as string}/public/${
                creatingSound._id
              }.mp3`,
              Body: stream
            } as S3.Types.PutObjectRequest,
            (err, data) => {
              if (!err) {
                if (creatingSound) {
                  creatingSound.location = data.Location;
                  creatingSound.save();
                }
              }
            }
          );

          user.state = States.WAITING_FOR_TRANSCRIPTION;
          user.save();
        } else {
          user.state = States.NONE;
          user.save();
          reject("Sorry, can't let you do that.");
        }

        resolve(
          "Lovely ! Now please provide me with some transcription of sound," +
            "so I'll be able to search for it in inline mode"
        );
      } else {
        user.state = States.NONE;
        user.save();
        reject("Sorry, can't let you do that.");
      }
    } else {
      reject("It is not an audio.");
    }
  });
}

export function HandleStateTranscript(
  user: UserDocument,
  msg: TelegramBot.Message
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    if (msg.text) {
      if (user.isManager) {
        const creatingSound = user.sounds.find(
          sound => sound.status === STATUS_CREATING
        );

        if (creatingSound) {
          creatingSound.transcription = msg.text;
          creatingSound.status = STATUS_CREATED;
          creatingSound.save();
          user.state = States.NONE;
          user.save();
        } else {
          user.state = States.NONE;
          user.save();
          reject("No sound is currently creating");
        }

        resolve("Finally ! Now I can easily get you this sound via search !");
      } else {
        user.state = States.NONE;
        user.save();
        reject("Sorry, can't let you do that.");
      }
    } else {
      reject("It is not a text at all.");
    }
  });
}
