import mongoose, { Schema } from "mongoose";
import { UserDocument } from "./User";

export const STATUS_CREATING = "STATUS_CREATING";
export const STATUS_CREATED = "STATUS_CREATED";

export type SoundDocument = mongoose.Document & {
  name: string;
  location: string;
  origin: string;
  transcription: string;
  status: string;
  manager: UserDocument;
};

const soundSchema = new mongoose.Schema({
  name: String,
  location: String,
  origin: String,
  transcription: String,
  status: {
    type: String,
    default: STATUS_CREATING
  },
  manager: {
    type: Schema.Types.ObjectId,
    ref: "User"
  }
});

export const Sound = mongoose.model<SoundDocument>(
  "Sound",
  soundSchema,
  "sounds"
);
