import mongoose, { Schema } from "mongoose";
import { SoundDocument } from "./Sound";
import logger from "../utils/logger";

export enum States {
  WAITING_FOR_NAME = "WAITING_FOR_NAME",
  WAITING_FOR_ORIGIN = "WAITING_FOR_ORIGIN",
  WAITING_FOR_AUDIO = "WAITING_FOR_AUDIO",
  WAITING_FOR_TRANSCRIPTION = "WAITING_FOR_TRANSCRIPTION",
  NONE = "WAITING_NONE"
}

export type UserDocument = mongoose.Document & {
  username: string;
  chatId: number;
  state: string;
  isManager: boolean;
  sounds: SoundDocument[];
};

const userSchema = new mongoose.Schema({
  username: String,
  chatId: {
    type: Number,
    unique: true
  },
  state: {
    type: String,
    default: States.NONE
  },
  isManager: {
    type: Boolean,
    default: false
  },
  sounds: [
    {
      type: Schema.Types.ObjectId,
      ref: "Sound"
    }
  ]
});

export const User = mongoose.model<UserDocument>("User", userSchema, "users");

userSchema.statics.someMethod = function(): void {
  logger.info("test");
};
